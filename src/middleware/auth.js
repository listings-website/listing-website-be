const jwt = require("jsonwebtoken");
require("dotenv/config");

module.exports = function (req, res, next) {
  //Get Token from header
  const token = req.header("x-auth-token");

  //check if token
  if (!token) {
    return res.status(401).json({ msg: "No token found" });
  }

  //verify token
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    req.user = decoded.user;

    next();
  } catch (err) {
    console.error(err.message);
    return res.status(401).json({ msg: "Token is invalid" });
  }
};
