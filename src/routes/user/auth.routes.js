const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
require("dotenv/config");

//user model
const User = require("../../models/User");

//middlewares
const authMiddleware = require("../../middleware/auth");

//@route  POST /user
//desc    authenticate user and return user
//access  public
router.post(
  "/signin",
  [
    check("email", "Email is required").isEmail(),
    check("password", "password is required").isLength({ min: 4 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const { email, password } = req.body;
      // get user by email
      const user = await User.findOne({ email });

      if (!user) {
        return res.status(404).json({ errors: "User not found" });
      }

      const isPasswordValid = await bcrypt.compare(password, user.password);

      if (!isPasswordValid) {
        return res.status(400).json({ errors: "Password is not valid" });
      }

      // sign jwt token
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: 3000000000 },
        (err, token) => {
          if (err) throw err;
          res.json({
            token,
            ...user._doc,
          });
        }
      );
    } catch (err) {
      console.log(err);
      res.status(404).send(err);
    }
  }
);

//@route  POST /signup
//desc    register user
//access  public
router.post(
  "/signup",
  [
    check("name", "Name is required").not().isEmpty(),
    check("email", "Email is required").isEmail(),
    check(
      "password",
      "Password is required to minimum of 4 characters"
    ).isLength({ min: 4 }),
  ],
  async (req, res) => {
    // validating req.body uing express-validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    try {
      //check if the user exists in the database

      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "user already exists" }] });
      }

      // create an instance of the user

      user = new User({
        name,
        email,
        password,
      });

      //encrypt password using bcrypt

      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      // save user
      await user.save();

      //return jsonwebtoken
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: 3000000000 },
        (err, token) => {
          if (err) throw err;
          res.json({
            token,
          });
        }
      );
    } catch (error) {
      console.log(error.message);
      res.status(500).send("internal server error");
    }
  }
);

module.exports = router;
