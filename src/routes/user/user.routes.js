const express = require("express");
const router = express.Router();
require("dotenv/config");

//user model
const User = require("../../models/User");

//middlewares
const authMiddleware = require("../../middleware/auth");

//@route  POST /user
//desc    authenticate user and return user
//access  public
router.get("/", authMiddleware, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select("-password");
    res.json({ ...user._doc });
  } catch (err) {
    console.log(err);
    res.status(404).send(err);
  }
});

module.exports = router;
