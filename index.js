const express = require("express");
const connectDb = require("./src/config/db");
const bodyParser = require("body-parser");

const app = express();

const PORT = process.env.PORT || 5000;

//init middleware
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

// import routes
const authRoutes = require("./src/routes/user/auth.routes");
const userRoutes = require("./src/routes/user/user.routes");

app.use("/auth", authRoutes);
app.use("/user", userRoutes);

//Routes
app.get("/", (req, res) => {
  res.send("We are here");
});

// Listen
app.listen(PORT, () => {
  // Connect to the Data base
  connectDb();

  console.log("Server listening on port " + PORT);
});
